import os
import re
import urllib2

from fabric.colors import green, white
from fabric.api import cd, sudo, task

DEFAULT_INSTALL_PATH = '/usr/share/minecraft'

print(white('\nDigital Ocean Minecraft Server Fabric file\n\n', bold=True))


@task
def install():
    """
    Perform a complete install of Minecraft on a Digital Ocean host
    """
    install_dependencies()
    install_minecraft_server()
    create_swapfile()
    display_instructions()


@task
def install_dependencies():
    """
    Install OS dependencies
    """
    print(green('Installing OS dependencies', bold=True))
    sudo('apt-get update')
    sudo('apt-get install -y wget default-jre-headless screen')


@task
def install_minecraft_server():
    """
    Download and install the minecraft_server.jar
    """
    print(green('Installing Minecraft server', bold=True))
    sudo('mkdir %s' % DEFAULT_INSTALL_PATH)
    download_page = urllib2.urlopen('https://minecraft.net/download').read()
    download_url = re.compile('"https://.*minecraft_server.[0-9\.]*.jar"').search(download_page).group()[1:-1]
    with cd(DEFAULT_INSTALL_PATH):
        sudo('wget -c %s -O minecraft_server.jar' % download_url)
        sudo('echo "java -Xmx512M -Xms512M -jar minecraft_server.jar nogui" > server.sh')
        sudo('chmod +x server.sh')
        sudo('echo "cd %s" > start_detached.sh' % DEFAULT_INSTALL_PATH)
        sudo('echo "screen -S MC -d -m ./server.sh" >> start_detached.sh')
        sudo('chmod +x start_detached.sh')

    # Remove the last line of /etc/rc.local -> exit 0
    sudo('sed -e \'$d\' /etc/rc.local > /tmp/rc.local')
    sudo('cp /tmp/rc.local /etc/rc.local')
    # Add line to autolaunch minecraft server on boot
    sudo('echo "%s" >> /etc/rc.local' % os.path.join(DEFAULT_INSTALL_PATH, 'start_detached.sh'))


@task
def create_swapfile():
    """
    Create and activate a swap file, taken from
    https://www.digitalocean.com/community/articles/how-to-add-swap-on-ubuntu-12-04
    """
    print(green('Creating swap file', bold=True))
    sudo('dd if=/dev/zero of=/swapfile bs=1024 count=512k')
    sudo('mkswap /swapfile')
    sudo('swapon /swapfile')
    sudo('echo "/swapfile       none    swap    sw      0       0" >> /etc/fstab')
    sudo('sysctl vm.swappiness=10')
    sudo('echo "vm.swappiness = 10" >> -a /etc/sysctl.conf')
    sudo('chown root:root /swapfile')
    sudo('chmod 0600 /swapfile')

@task
def uninstall_minecraft_server():
    sudo('rm %s -Rf' % DEFAULT_INSTALL_PATH)


@task
def display_instructions():
    print(white('To reattach to the Minecraft server console use: screen -S MC -d -r'))
