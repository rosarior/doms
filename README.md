### Digital Ocean Minecraft Server Fabric File
An automated way to deploy a minecraft server on a Digital Ocean's VPS.

### How to use

```
wget https://github.com/rosarior/doms/raw/master/fabfile.py && fab -H <ip address> -u root -p <root password> install
```

Once finished just connect to your new Minecraft server using it's IP address.

To connect to the Minecraft server console:

* ssh into your server
* type: `screen -S MC -d -r`
* to detach from the console, press: Ctrl+a d

### Don't forget to use my referral key to launch your <a href="https://www.digitalocean.com/?refcode=21afe7aa495d">$5 a month Minecraft server</a>
